import os
import csv
import pandas as pd
import numpy as np

from gensim import corpora
from gensim import utils as gen_utils
from gensim.parsing.porter import PorterStemmer

#from utils.data import prepare_text_classification_data

from utils.text_vectorization import WordVectorizer, Word2VecVectorizer, FastTextVectorizer, Doc2VecVectorizer,\
    ELMoVectorizer, InferSentVectorizer, TransformerVectorizer, DataIter
# from utils.text_vectorization import ELMoVectorizer, USEVectorizer

from tqdm import tqdm

def prepare_text_classification_data(file_path):
    df = pd.read_csv(file_path)
    assert "text" in df.columns, "'text' column does not exist in dataset!"
    assert "label" in df.columns, "'label' column does not exist in dataset!"

    print("Label count in the dataset:")
    print(df['label'].value_counts())

    # Tokenize the text column to get the new column 'tokenized_text'
    #df['tokenized_text'] = [gen_utils.simple_preprocess(line, deacc=True) for line in df['text']]

    #porter_stemmer = PorterStemmer()
    # Get the stemmed_tokens
    #df['stemmed_tokens'] = [[porter_stemmer.stem(word) for word in tokens] for tokens in
    #                                 df['tokenized_text']]

    #df["sentences"] = [" ".join(tokens) for tokens in df["stemmed_tokens"]]

    return df


def vectorize_normal(files_to_vectorize, vectorizer, vectorizer_name):
    for file in files_to_vectorize:
        print("Loading data from {}".format(file))
        df = prepare_text_classification_data(file)
        file_name = os.path.splitext(file)[0].split("/")[-1]
        output_file = "vectorization/{}_{}.csv".format(file_name, vectorizer_name)

        with open(output_file, "w") as write_file:
            #writer = csv.writer(write_file)
            #writer.writerow(("text", "vector", "label"))
            for row in tqdm(df.itertuples(index=False), total=df.shape[0]):
                tokens = list(gen_utils.tokenize(row[0], lower=True))
                vec = vectorizer.vectorize(tokens)
                # vec_str = " ".join([str(elem) for elem in vec])
                try:
                    if not type(vec) == np.float64:
                        # writer.writerow((row["text"], vec, row["label"]))

                        write_file.write("\"{}\",\"".format(row[0]))

                        for el in vec:
                            write_file.write("{} ".format(el))

                        write_file.write("\",{}".format(row[1]))

                        #if is_ae:
                        #    f.write(",\"{}\"".format(row[3]))

                        write_file.write("\n")
                except Exception as ex:
                    print("ERROR")
                    print(ex)
                    print()
                    continue


def vectorize_perturbed(file, vectorizer, vectorizer_name):
    print("Loading data from {}".format(file))
    df = pd.read_csv(file)
    output_file = "vectorization/transformed_sentences.csv"
    # sentence,vector,perturb_method,difficulty,model

    with open(output_file, "a") as write_file:
        writer = csv.writer(write_file)
        for index, row in df.iterrows():
            tokens = list(gen_utils.tokenize(row["sentence"], lower=True))
            vec = vectorizer.vectorize(tokens)
            writer.writerow((row["sentence"], vec,
                             row["perturb_method"],
                             row["difficulty"],
                             vectorizer_name))


files_to_vectorize = ["external-repos/txt-autoencoders/data/imdb/train_imdb.txt",
                      "external-repos/txt-autoencoders/data/imdb/val_imdb.txt",
                      "external-repos/txt-autoencoders/data/imdb/test_imdb.txt"]
perturb_file = "jupyter/perturbed_sentences.csv"

#infersent_params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
#                          'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}

#imdb_iter = DataIter("train_imdb_without_sentiment.txt")


vectorizers = [(WordVectorizer, {"pretrained": "glove-twitter-200"}, "glove"),
               #(WordVectorizer, {"pretrained": "fasttext-wiki-news-subwords-300"}, "fasttext"),
               #(Doc2VecVectorizer, {"path_to_model": "jupyter/imdb_doc2vec.model"}, "doc2vec"),
               #(InferSentVectorizer, {"sentences": imdb_iter,
               #                       "path_to_model": "utils/InferSent/encoder/infersent2.pkl",
               #                       "params_model": infersent_params_model,
               #                       "path_to_w2v": "utils/InferSent/fastText/crawl-300d-2M.vec"}, "infersent"),
               #(TransformerVectorizer, {"path_to_model": "paraphrase-distilroberta-base-v1"}, "transformer")
                ]


#vectorizers = [(ELMoVectorizer, {"path_to_model": "https://tfhub.dev/google/elmo/3"}, "elmo"),
#               (USEVectorizer, {"path_to_model": "https://tfhub.dev/google/universal-sentence-encoder/4"}, "use")]

if not os.path.isdir("vectorization"):
    os.mkdir("vectorization")

for config in vectorizers:
    print("Loading vectorizer {}...".format(config[2]))
    # Create the vectorizer using the config
    vectorizer = config[0](config[1])

    vectorize_normal(files_to_vectorize, vectorizer, config[2])
    vectorize_perturbed(perturb_file, vectorizer, config[2])

