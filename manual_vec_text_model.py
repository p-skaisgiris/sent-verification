import os
import pickle
import sys
from collections import OrderedDict
import numpy as np
import copy

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from utils.data import prepare_yelp_train_test, split_train_test,\
    train_bow_vectorizer, train_tfidf_vectorizer, prepare_text_classification_data, TextClassificationDataset
from utils.networks import SmallFFNet, SmallLinearizedFFNet,\
    train_model, evaluate_model
from utils.pytorch_pwl_functions import PWLApprox, sigmoid
from utils.onnx import torch_to_onnx

# label_function = make_target_binary

# Use cuda if present
#device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = "cpu"

# Create directories to neatly store results
if not os.path.isdir("models"):
    os.mkdir("models")
if not os.path.isdir("models/train_log"):
    os.mkdir("models/train_log")
if not os.path.isdir("models/pytorch"):
    os.mkdir("models/pytorch")
if not os.path.isdir("models/onnx"):
    os.mkdir("models/onnx")
if not os.path.isdir("models/nnet"):
    os.mkdir("models/nnet")

# -1 for negative, 0 for neutral, and 1 for positive
#NUM_LABELS = 3
# -1 for negative, and 1 for positive
NUM_LABELS = 2

# ============= DEFINE MODEL =================
output_dim = NUM_LABELS

num_epochs = 20

# Parameters for the linearized approx
lower_bound = -5
upper_bound = 5
segments = 10

batch_size = 1024

exp_datasets = [("vectorization/train_imdb_glove.csv",
                 "vectorization/val_imdb_glove.csv",
                 "vectorization/test_imdb_glove.csv"),
                ("vectorization/train_imdb_fasttext.csv",
                 "vectorization/val_imdb_fasttext.csv",
                 "vectorization/test_imdb_fasttext.csv"),
                ("vectorization/train_imdb_doc2vec.csv",
                 "vectorization/val_imdb_doc2vec.csv",
                 "vectorization/test_imdb_doc2vec.csv"),
                ("vectorization/train_imdb_transformer.csv",
                 "vectorization/val_imdb_transformer.csv",
                 "vectorization/test_imdb_transformer.csv"),
                ("vectorization/train_imdb_use.csv",
                 "vectorization/val_imdb_use.csv",
                 "vectorization/test_imdb_use.csv"),
                ("vectorization/train_imdb_aae.csv",
                 "vectorization/val_imdb_aae.csv",
                 "vectorization/test_imdb_aae.csv"),
                ("vectorization/train_imdb_daae.csv",
                 "vectorization/val_imdb_daae.csv",
                 "vectorization/test_imdb_daae.csv"),
                ("vectorization/train_imdb_infersent.csv",
                 "vectorization/val_imdb_infersent.csv",
                 "vectorization/test_imdb_infersent.csv")
                ]
#exp_layers = [5]
#exp_nodes = [25]
# exp_act_fns = [nn.ReLU()]
#exp_act_fns = [PWLApprox(np.tanh, lower_bound, upper_bound, segments, device),
#               nn.Tanh(),
#               nn.ReLU()]

early_stopping_thresh = 0.001
early_stop_epochs = 3

learning_rate = 0.1


exp_layers = [2, 5, 10]

exp_nodes = [5, 10, 25, 100]

exp_act_fns = [nn.Tanh(),
               PWLApprox(np.tanh, lower_bound, upper_bound, segments, device),
               nn.Sigmoid(),
               PWLApprox(sigmoid, lower_bound, upper_bound, segments, device),
               nn.ReLU()]


# Change the layer type here, don't forget to adjust the sequential model's parameters probably
layer_type = nn.Linear

# ============== NOTE: THE CURRENT TRAINING IS ONLY FOR BINARY VALUES =======================

for dataset in exp_datasets:
    train_data = TextClassificationDataset(dataset[0])
    train_dataloader = DataLoader(train_data, batch_size=batch_size)

    val_data = TextClassificationDataset(dataset[1])
    val_dataloader = DataLoader(val_data, batch_size=batch_size)

    test_data = TextClassificationDataset(dataset[2])
    test_dataloader = DataLoader(test_data, batch_size=batch_size)

    # Get the first datapoint from the train dataset and check it's size
    emb_dim = len(next(iter(train_dataloader))[0][0])

    encoder_name = os.path.splitext(dataset[0])[0].split("/")[-1].split("_")[-1]

    for num_layers in exp_layers:
        for hidden_dim in exp_nodes:
            for activation_function in exp_act_fns:
                model_dict = OrderedDict()
                model_dict["layer0"] = layer_type(emb_dim, hidden_dim)
                model_dict["act0"] = activation_function
                i = 0
                for i in range(1, num_layers-1):
                    model_dict["layer{}".format(i)] = layer_type(hidden_dim, hidden_dim)
                    model_dict["act{}".format(i)] = copy.copy(activation_function)
                model_dict["layer{}".format(i+1)] = layer_type(hidden_dim, output_dim)

                """
                layers = OrderedDict([
                    ('layer0', nn.Linear(emb_dim, hidden_dim)),
                    ('act0', nn.ReLU()),
                    ('layer1', nn.Linear(hidden_dim, int(hidden_dim/2))),
                    ('act1', nn.ReLU()),
                    ('layer2', nn.Linear(int(hidden_dim/2), output_dim)),
                ])
                """

                model = nn.Sequential(model_dict)

                # Convert model to GPU
                model.to(device)

                # weight=torch.tensor([0.9533527061387577, 1], device=device)
                loss_function = nn.CrossEntropyLoss()
                optimizer = optim.Adam(model.parameters(), lr=learning_rate)

                model_name = "ffn_{}_{}_{}_{}_{}_{}".format(encoder_name,
                                                      num_layers,
                                                      hidden_dim,
                                                      type(activation_function).__name__,
                                                      type(optimizer).__name__,
                                                      learning_rate)
                print("Dataset: {}".format(dataset[0]))
                print("No. layers: {}".format(num_layers))
                print("Hidden dimensions: {}".format(hidden_dim))
                print("Activation function: {}".format(type(activation_function).__name__))
                print("Optimizer: {}".format(type(optimizer).__name__))
                print("Learning rate: {}".format(learning_rate))

                print()
                print("Training model...")
                train_model(model=model,
                            optimizer=optimizer,
                            loss_function=loss_function,
                            train_dataloader=train_dataloader,
                            val_dataloader=val_dataloader,
                            num_epochs=num_epochs,
                            model_name=model_name,
                            early_stop_thresh=early_stopping_thresh,
                            early_stop_epochs=early_stop_epochs,
                            device=device)

                torch_to_onnx(model, batch_size, emb_dim, model_name, device)

                print("Evaluating model...")
                evaluate_model(model=model,
                               data=test_dataloader,
                               model_loss_filename="models/train_log/{}.csv".format(model_name),
                               device=device)
