import nltk
from collections import defaultdict
from scipy.spatial import distance
import numpy as np


def compute_recall(sentences, embeddings, k=10, char_level=True):
    """Function that computes recall as defined in "Educating Text AutoEncoders:
     Latent Representation Guidance via Denoising" Shen et al., 2020

    Args:
        sentences: list of strings
        embeddings: list of their representations in latent space
        k: amount of nearest neighbours in discrete and latent space to take into account

    Returns: the mean of all recall scores for each sentence
    """
    assert len(sentences) == len(embeddings), "The length of list of sentences does not " \
                                              "equal length of list of their embeddings!"

    if char_level:
        def char_level_ned(a, b):
            return nltk.edit_distance(a, b) / max(len(a), len(b))

        normalized_edit_distance = char_level_ned
    else:
        def token_level_ned(a, b):
            tokens_a = a.split()
            tokens_b = b.split()
            return nltk.edit_distance(tokens_a, tokens_b) / max(len(tokens_a), len(tokens_b))

        normalized_edit_distance = token_level_ned

    res = defaultdict(list)

    # For each sentence pairs, compute their normalized edit distance in the discrete space
    # and euclidean distance in the latent space
    for i in range(len(sentences)):
        for j in range(len(sentences)):
            # Do not compute distances for the same sentence
            if i == j:
                continue
            sent_i = sentences[i]
            sent_j = sentences[j]
            n_l_distance = normalized_edit_distance(sent_i, sent_j)
            eucl_distance = distance.euclidean(embeddings[i], embeddings[j])
            res[sent_i].append((sent_j, n_l_distance, eucl_distance))

    recalls = []
    for key in res.keys():
        # Sort ascending (we want to take k nearest neighbours)
        sorted_n_l = sorted(res[key], key=lambda x: x[1])
        sorted_eucl = sorted(res[key], key=lambda x: x[2])

        # Filter top k nearest neighbours
        sorted_n_l = sorted_n_l[:k]
        sorted_eucl = sorted_eucl[:k]

        # Put the nearest neighbour strings into sets for easier comparison
        top_k_n_l = set()
        top_k_eucl = set()
        for i in range(k):
            top_k_n_l.add(sorted_n_l[i][0])
            top_k_eucl.add(sorted_eucl[i][0])

        intersection = len(top_k_n_l.intersection(top_k_eucl))
        recall = intersection / k
        recalls.append(recall)

    return np.mean(recalls)


if __name__ == "__main__":
    sentences = ["hello there", "hello world", "i like mayo", "i like ketchup", "woop", "hippie",
                 "hippopotamus is walking like a bottle of ketchup"]
    embeddings = [[1, 2, 3], [2, 3, 4], [4, 5, 6], [6, 7, 8], [1, 5, 8], [3, 2, 1], [6, 1, 1]]

    compute_recall(sentences, embeddings, 3)



