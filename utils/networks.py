import time
import csv

import numpy as np

import torch
import torch.nn as nn

from utils.pytorch_pwl_functions import PWLApprox

from tqdm import tqdm

from sklearn.metrics import confusion_matrix


def is_cuda():
    return torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class SmallRNN(nn.Module):
    def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim):
        super(SmallRNN, self).__init__()

        self.embedding = nn.Embedding(input_dim, embedding_dim)

        self.rnn = nn.RNN(embedding_dim, hidden_dim)

        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, text):
        # text = [sent len, batch size]

        embedded = self.embedding(text)

        # embedded = [sent len, batch size, emb dim]

        output, hidden = self.rnn(embedded)

        # output = [sent len, batch size, hid dim]
        # hidden = [1, batch size, hid dim]

        assert torch.equal(output[-1, :, :], hidden.squeeze(0))

        return self.fc(hidden.squeeze(0))


class SmallFFNet(nn.Module):
    """Simple small feedforward neural-network network."""

    def __init__(self, input_dim, hidden_dim, output_dim):
        super(SmallFFNet, self).__init__()

        # Linear function (input)
        self.fc1 = nn.Linear(input_dim, hidden_dim)

        # Non-linearity
        self.relu = nn.ReLU()

        # Linear function (output)
        self.fc2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # Linear function
        out = self.fc1(x)

        # Non-linearity
        out = self.relu(out)

        out = self.fc2(out)
        #out = out.squeeze()
        return out


class SmallLinearizedFFNet(nn.Module):
    """Simple small feedforward neural-network network with linearized activation functions."""

    def __init__(self, input_dim, hidden_dim, output_dim, fn, lower_bound, upper_bound, segments):
        super(SmallLinearizedFFNet, self).__init__()

        # Linear function (input)
        self.fc1 = nn.Linear(input_dim, hidden_dim)

        # Non-linearity
        self.ac1 = PWLApprox(fn, lower_bound, upper_bound, segments)

        # Linear function (output)
        self.fc2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # Linear function
        out = self.fc1(x)

        # Non-linearity
        out = self.ac1(out)

        out = self.fc2(out)
        #out = out.squeeze()
        return out


class SmallEmbFFNet(nn.Module):
    """
    def __init__(self, model_param: ModelParam):
        super().__init__()
        self.embedding = nn.Embedding(
            model_param.vocab_size,
            model_param.embedding_dim
        )
        self.lin = nn.Linear(
            model_param.input_size * model_param.embedding_dim,
            model_param.target_dim
        )

    def forward(self, x):
        #features = self.embedding(x).view(x.size()[0], -1)

        features = F.relu(features)
        features = self.lin(features)
        return features

    #Simple small feedforward neural-network network.
"""
    def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim):
        super(SmallEmbFFNet, self).__init__()

        self.embedding = nn.Embedding(input_dim, embedding_dim)

        # Linear function (input)
        self.fc1 = nn.Linear(embedding_dim, hidden_dim)

        # Non-linearity
        self.relu = nn.ReLU()

        # Linear function (output)
        self.fc2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # text = [sent len, batch size]

        embedded = self.embedding(x)

        # Linear function
        out = self.fc1(embedded)

        # Non-linearity
        out = self.relu(out)

        out = self.fc2(out)
        return out


class MediumFFNet(nn.Module):
    """Simple medium-sized feedforward neural-network network."""

    def __init__(self, input_dim, hidden_dim, output_dim):
        super(MediumFFNet, self).__init__()

        # Linear function 1: vocab_size --> hidden_dim
        self.fc1 = nn.Linear(input_dim, hidden_dim)
        # Non-linearity 1
        self.relu1 = nn.ReLU()

        # Linear function 2: hidden_dim --> hidden_dim
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)
        # Non-linearity 2
        self.relu2 = nn.ReLU()

        # Linear function 3 (readout): hidden_dim --> output_dim
        self.fc3 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # Linear function 1
        out = self.fc1(x)
        # Non-linearity 1
        out = self.relu1(out)

        # Linear function 2
        out = self.fc2(out)
        # Non-linearity 2
        out = self.relu2(out)

        # Linear function 3 (readout)
        out = self.fc3(out)
        return out


class LargeFFNet(nn.Module):
    """Simple large feedforward neural-network network."""

    def __init__(self, input_dim, hidden_dim, output_dim):
        super(LargeFFNet, self).__init__()

        # Linear function 1: vocab_size --> hidden_dim
        self.fc1 = nn.Linear(input_dim, hidden_dim)
        # Non-linearity 1
        self.relu1 = nn.ReLU()

        # Linear function 2: hidden_dim --> hidden_dim
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)
        # Non-linearity 2
        self.relu2 = nn.ReLU()

        # Linear function 3: hidden_dim --> hidden_dim
        self.fc3 = nn.Linear(hidden_dim, hidden_dim)
        # Non-linearity 3
        self.relu3 = nn.ReLU()

        # Linear function 4 (readout): hidden_dim --> output_dim
        self.fc4 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # Linear function 1
        out = self.fc1(x)
        # Non-linearity 1
        out = self.relu1(out)

        # Linear function 2
        out = self.fc2(out)
        # Non-linearity 2
        out = self.relu2(out)

        # Linear function 3
        out = self.fc3(out)
        # Non-linearity 3
        out = self.relu3(out)

        # Linear function 4 (readout)
        out = self.fc4(out)
        return out


def print_train_parameters(model):
    num_param = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(f'The model has {num_param:,} trainable parameters')


def train_model(model, optimizer, loss_function, train_dataloader, val_dataloader,
                num_epochs, model_name, early_stop_thresh, early_stop_epochs, device="cpu"):
    print_train_parameters(model)
    # Open the file for writing various metrics
    model_loss_filename = 'models/train_log/{}.csv'.format(model_name)
    f = open(model_loss_filename, 'w')
    writer = csv.writer(f)
    writer.writerow(("iter", "train_loss", "valid_loss", "train_acc", "val_acc", "train_time"))
    min_valid_loss = np.inf

    total_train_iters = 0
    total_val_iters = 0
    total_train = 0
    total_val = 0
    no_change_loss = 0
    # Start training
    for epoch in range(num_epochs):
        train_loss = 0
        train_correct = 0
        model.train()
        start_time = time.time()
        #i = 0
        for input, label in tqdm(train_dataloader):
            input = input.to(device)
            label = label.to(device)

            # Clearing the accumulated gradients
            optimizer.zero_grad()

            # Forward pass to get output
            probs = model(input.float())

            # Calculate Loss
            loss = loss_function(probs, label)
            # Accumulating the loss over time
            train_loss += loss.item()

            # Getting gradients w.r.t. parameters
            loss.backward()

            # Updating parameters
            optimizer.step()

            preds = torch.argmax(probs, dim=1)
            # Convert into float for summation and division
            train_correct += (preds == label).float().sum()

            # train_accuracy += acc.item()

            if epoch == 0:
                total_train_iters += 1
                total_train += label.size(0)

            #i+= 1
            #if i == 5:
            #    break



        end_time = time.time()

        valid_loss = 0
        val_correct = 0
        model.eval()
        # Compute valid loss
        # No need to compute gradients, minor speedup
        #j = 0
        with torch.no_grad():
            for val_input, val_label in tqdm(val_dataloader):
                val_input = val_input.to(device)
                val_label = val_label.to(device)

                probs = model(val_input.float())
                # Calculate Loss
                loss = loss_function(probs, val_label)
                # Accumulating the loss over time
                valid_loss += loss.item()

                preds = torch.argmax(probs, dim=1)
                # Convert into float for summation and division
                val_correct += (preds == val_label).float().sum()

                if epoch == 0:
                    total_val_iters += 1
                    total_val += val_label.size(0)

                #j += 1
                #if j == 5:
                #    break

        elapsed_time = end_time - start_time
        train_loss = train_loss / total_train_iters
        valid_loss = valid_loss / total_val_iters
        train_accuracy = 100 * train_correct / total_train
        train_accuracy = train_accuracy.item()
        valid_accuracy = 100 * val_correct / total_val
        valid_accuracy = valid_accuracy.item()
        print('Epoch: {}/{}. Train time: {} '
              'Training Loss: {:.6f} Validation loss: {:.6f} '
              'Train acc: {:.6f} Val acc: {:.6f} '.format(epoch + 1, num_epochs, elapsed_time,
                                                          train_loss, valid_loss,
                                                          train_accuracy, valid_accuracy))
        writer.writerow((epoch + 1, train_loss, valid_loss, train_accuracy, valid_accuracy, elapsed_time))

        # Check early stopping condition
        if min_valid_loss - valid_loss < early_stop_thresh:
            no_change_loss += 1
        else:
            no_change_loss = 0

        if no_change_loss == early_stop_epochs:
            print("Loss increased less than {} for {} epochs. Doing early stopping...".format(early_stop_thresh,
                                                                                              early_stop_epochs))
            break

        if min_valid_loss > valid_loss:
            print(f'Validation Loss Decreased({min_valid_loss:.6f}--->{valid_loss:.6f}) \t Saving The Model')
            min_valid_loss = valid_loss
            # Saving State Dict
            torch.save(model.state_dict(), 'models/pytorch/{}.pt'.format(model_name))

    f.close()


def evaluate_model(model, data, model_loss_filename, device="cpu"):
    confusion_matrices = []
    with torch.no_grad():
        for input, label in data:
            input = input.to(device)

            probs = model(input.float())
            preds = torch.argmax(probs, dim=1).cpu()
            confusion_matrices.append(confusion_matrix(label, preds).ravel())
            # acc = binary_accuracy_batch(probs, label)
            # accuracy += acc.item()
    # tn, fp, fn, tp
    final_cm = sum(confusion_matrices)
    accuracy = (final_cm[3] + final_cm[0]) / sum(final_cm)
    print("Test accuracy: {}".format(accuracy))
    with open(model_loss_filename, "a") as f:
        f.write("test,{},{},{},{},".format(final_cm[0], final_cm[1], final_cm[2], final_cm[3]))


def binary_accuracy_batch(probs, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """
    #round predictions to the closest integer
    #rounded_preds = torch.round(torch.sigmoid(preds))
    preds = torch.argmax(probs, dim=1)
    # Convert into float for summation and division
    correct = (preds == y).float()
    acc = correct.sum() / len(correct)
    return acc


def binary_accuracy(model, dataloader, device):
    correct_list = []
    for input, label in dataloader:
        probs = model(input)
        pred_ind = torch.argmax(probs, dim=1).cpu().numpy()[0]
        correct_list.append(pred_ind == label)
    acc = correct_list.count(True) / len(correct_list)
    return acc


def train_model_iterator(model, iterator, optimizer, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.train()

    for batch in iterator:
        optimizer.zero_grad()

        predictions = model(batch.text).squeeze(1)

        loss = criterion(predictions, batch.label)

        acc = binary_accuracy_batch(predictions, batch.label)

        loss.backward()

        optimizer.step()

        epoch_loss += loss.item()
        epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate_model_iterator(model, iterator, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.eval()

    with torch.no_grad():
        for batch in iterator:
            predictions = model(batch.text).squeeze(1)

            loss = criterion(predictions, batch.label)

            acc = binary_accuracy_batch(predictions, batch.label)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs