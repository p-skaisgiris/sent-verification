"""
    File for creating piecewise-linear activation function in PyTorch

    Largely inspired by:
    https://uvadlc-notebooks.readthedocs.io/en/latest/tutorial_notebooks/tutorial3/Activation_Functions.html
"""

import numpy as np

import matplotlib.pyplot as plt

import torch
import torch.nn as nn


class ActivationFunction(nn.Module):
    def __init__(self):
        super().__init__()
        self.name = self.__class__.__name__
        self.config = {"name": self.name}


class PWLApprox(ActivationFunction):
    def __init__(self, fn, lower_bound, upper_bound, segments, device='cpu'):
        """Activation function of a piecewise-linear approximation of a smooth function

        Args:
            fn:
            lower_bound:
            upper_bound:
            segments:
        """
        super().__init__()
        ms, bs, step = linearize(fn, lower_bound, upper_bound, segments)
        self.config["slope"] = torch.Tensor(ms).to(device=device)
        self.config["intercept"] = torch.Tensor(bs).to(device=device)
        self.config["lower_bound"] = lower_bound
        self.config["upper_bound"] = upper_bound
        self.config["step"] = step
        self.fn_name = fn.__name__

    def forward(self, x):
        #idx = torch.floor((x - self.config["lower_bound"]) / self.config["step"]) + 1
        idx = torch.where(x > self.config["upper_bound"], -1.0,
                          torch.where(x < self.config["lower_bound"], 0.0,
                                      # + 1 because functions[0] is defined for x < minimum
                                      (torch.floor((x - self.config["lower_bound"]) / self.config["step"]) + 1).double()))
        # Convert to LongTensor because only those can be used as indices for tensors
        idx = idx.type(torch.LongTensor)
        return linear(x, self.config["slope"][idx], self.config["intercept"][idx])
        """

        return torch.where(x > self.config["upper_bound"],
                           linear(x, self.config["slope"][-1], self.config["intercept"][-1]),
                           torch.where(x < self.config["lower_bound"],
                                       linear(x, self.config["slope"][0], self.config["intercept"][0]),
                                       linear(x, self.config["slope"][idx], self.config["intercept"][idx])))
        """

    def __repr__(self):
        return "PWLApprox{}".format(self.fn_name)


def linear(x, m, b):
    return m*x + b


def linearize(fn, lower, upper, segments, plot=False):
    interval = upper - lower
    step = interval / segments

    # List of tuples (slope, slope intercept)
    ms = []
    bs = []

    x1 = lower
    x2 = x1 + step
    # Compute line segments
    while x1 <= upper:
        # Compute slope
        m = (fn(x2) - fn(x1)) / (x2 - x1)
        # Compute slope intercept
        b = -(x2*m) + fn(x2)
        ms.append(m)
        bs.append(b)
        x1 = x2
        x2 += step

    low_lower = lower - 5
    up_upper = upper + 5
    # Compute segment for all x < lower
    bs.insert(0, linear(lower, ms[0], bs[0]))
    ms.insert(0, 0)
    # Compute segment for all x > upper
    bs.append(linear(upper, ms[-1], bs[-1]))
    ms.append(0)

    if plot:
        # Plot ground truth
        ground_x = np.linspace(low_lower, up_upper, 100)
        ground_y = fn(ground_x)
        plt.plot(ground_x, ground_y, color="g", label="precise")

        pivots_x = []
        pivots_y = []
        x1 = lower
        x2 = x1 + step
        # Plot the separate sub-segments
        for i in range(1, len(ms)-2):
            m = ms[i]
            b = bs[i]
            y1 = linear(x1, m, b)
            y2 = linear(x2, m, b)
            pivots_x.append(x1)
            pivots_y.append(y1)
            plt.plot([x1, x2], [y1, y2], color="r")
            x1 = x2
            x2 += step
        pivots_x.append(x2-step)
        pivots_y.append(y2)

        # Plot function x < lower
        plt.plot([low_lower, lower],
                 [linear(low_lower, ms[0], bs[0]),
                  linear(lower, ms[1], bs[1])], color="r", label="approx.")
        # Plot function x > upper
        plt.plot([upper, up_upper],
                 [linear(upper, ms[-2], bs[-2]),
                  linear(up_upper, ms[-1], bs[-1])], color="r")

        # Plot pivot points
        plt.scatter(pivots_x, pivots_y, color="b", label="pivot points")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.grid(True)
        plt.legend()
        plt.show()

    # Could just give the points to matplotlib and let it do the rest
    #if plot:
    #    # Plot ground truth
    #    ground_x = np.linspace(lower, upper, 100)
    #    ground_y = fn(ground_x)
    #    plt.plot(ground_x, ground_y, color="g", label="precise")
    #    # Plot approximation
    #    plt.plot(pivots_x, pivots_y, color="r", label="approx.")
    #    # Plot pivot points
    #    plt.scatter(pivots_x, pivots_y, color="b", label="pivot points")
    #    plt.xlabel("x")
    #    plt.ylabel("y")
    #    plt.grid(True)
    #    plt.legend()
    #    plt.show()

    return ms, bs, step


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


if __name__ == "__main__":
    # linearize(sigmoid, -5, 5, 10, True)
    linearize(np.tanh, -5, 5, 10, True)
