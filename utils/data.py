import random

import torch
from torch.utils.data import IterableDataset

from torchtext import data, datasets, vocab

import pandas as pd
import numpy as np

from gensim import corpora
from gensim.utils import simple_preprocess
from gensim.parsing.porter import PorterStemmer

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

import pickle

import csv

class TextClassificationDataset(IterableDataset):
    def __init__(self, filename):
        # Store the filename in object's memory
        self.filename = filename

        # And that's it, we no longer need to store the contents in the memory

    def preprocess(self, text):
        ### Do something with text here
        text_pp = text.lower().strip()
        ###

        return text_pp

    def convert_str_to_vector(self, row):
        # a = row[1:-1].split(' ')
        a = row.split(' ')
        return np.array([float(b.strip()) for b in a if b != ""])

    def map_sentiment_to_num(self, row):
        if row == "positive":
            return 1
        elif row == "negative":
            return 0

    def parse_text_csv(self, line):
        #vec = self.convert_str_to_vector(line["vector"].iloc[0])
        #label = self.map_sentiment_to_num(line["label"].iloc[0])
        vec = self.convert_str_to_vector(line[1])
        label = self.map_sentiment_to_num(line[2])

        return vec, label

    def __iter__(self):
        # Create an iterator
        #file_itr = pd.read_csv(self.filename, chunksize=1)
        file_itr = open(self.filename)
        reader = csv.reader(file_itr, delimiter=",")
        next(reader)

        # Map each element using the line_mapper
        mapped_itr = map(self.parse_text_csv, reader)

        return mapped_itr

def load_data_with_torchtext(path, train_name, test_name, val_name, data_format, fields,
                             max_vocab_size=30000, batch_size=2, device="cpu", vectors_path=None):
    text = data.Field(sequential=True, use_vocab=True, tokenize='spacy', tokenizer_language='en_core_web_sm', lower=True)
    label = data.Field(sequential=False, use_vocab=False)

    train_data, test_data = data.TabularDataset.splits(path=path,
                                                       train=train_name,
                                                       test=test_name,
                                                       format=data_format,
                                                       fields=fields)

    if vectors_path:
        # Load custom embeddings
        vectors = vocab.Vectors(name=vectors_path)
    else:
        # Fallback to 100 dimensional GloVe vectors
        vectors = "glove.6B.100d"

    text.build_vocab(train_data,
                     max_size=max_vocab_size,
                     vectors=vectors)

    train_iterator, test_iterator = data.BucketIterator.splits(
        (train_data, test_data),
        batch_size=batch_size,
        device=device
    )


def load_imdb_data_iterators(max_vocab_size=25000, batch_size=64, device="cpu", verbose=False):
    SEED = 1234

    torch.manual_seed(SEED)
    torch.backends.cudnn.deterministic = True

    TEXT = data.Field(tokenize='spacy',
                      tokenizer_language='en_core_web_sm')
    LABEL = data.LabelField(dtype=torch.float)

    train_data, test_data = datasets.IMDB.splits(TEXT, LABEL)

    if verbose:
        print(f'Number of training examples: {len(train_data)}')
        print(f'Number of testing examples: {len(test_data)}')

    # print(vars(train_data.examples[0]))

    train_data, valid_data = train_data.split(random_state=random.seed(SEED))

    if verbose:
        print(f'Number of training examples: {len(train_data)}')
        print(f'Number of validation examples: {len(valid_data)}')
        print(f'Number of testing examples: {len(test_data)}')

    TEXT.build_vocab(train_data, max_size=max_vocab_size)
    LABEL.build_vocab(train_data)

    if verbose:
        print(f"Unique tokens in TEXT vocabulary: {len(TEXT.vocab)}")
        print(f"Unique tokens in LABEL vocabulary: {len(LABEL.vocab)}")

    train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
        (train_data, valid_data, test_data),
        batch_size=batch_size,
        device=device)

    return train_iterator, valid_iterator, test_iterator, TEXT, LABEL


def load_yelp_orig_data(yelp_path):
    data = []
    i = 0
    # read the entire file into a python array
    with open(yelp_path, 'r') as f:
        for line in f:
            data.append(line)
            i += 1
            if i == 100000:
                break
        #data = f.readlines()

    # remove the trailing "\n" from each line
    data = map(lambda x: x.rstrip(), data)

    data_json_str = "[" + ','.join(data) + "]"

    # now, load it into pandas
    data_df = pd.read_json(data_json_str)

    data_df.to_csv('data/output_reviews_top.csv')


def prepare_yelp_train_test(file_path, num_points):
    df = pd.read_csv(file_path)

    print("Number of rows per star rating:")
    print(df['stars'].value_counts())

    # Function to map stars to sentiment
    def map_sentiment(stars_received):
        if stars_received <= 2:
            return -1
        elif stars_received == 3:
            return 0
        else:
            return 1

    # Mapping stars to sentiment into three categories
    df['sentiment'] = [map_sentiment(x) for x in df['stars']]

    # Function call to get the top num_points from each sentiment
    top_data_df = get_top_data(df, top_n=num_points)

    # Tokenize the text column to get the new column 'tokenized_text'
    top_data_df['tokenized_text'] = [simple_preprocess(line, deacc=True) for line in top_data_df['text']]

    porter_stemmer = PorterStemmer()
    # Get the stemmed_tokens
    top_data_df['stemmed_tokens'] = [[porter_stemmer.stem(word) for word in tokens] for tokens in
                                           top_data_df['tokenized_text']]

    top_data_df["sentences"] = [" ".join(tokens) for tokens in top_data_df["stemmed_tokens"]]

    return top_data_df


def prepare_text_classification_data(file_path):
    df = pd.read_csv(file_path)
    assert "text" in df.columns, "'text' column does not exist in dataset!"
    assert "label" in df.columns, "'label' column does not exist in dataset!"

    print("Label count in the dataset:")
    print(df['label'].value_counts())

    # Tokenize the text column to get the new column 'tokenized_text'
    df['tokenized_text'] = [simple_preprocess(line, deacc=True) for line in df['text']]

    porter_stemmer = PorterStemmer()
    # Get the stemmed_tokens
    df['stemmed_tokens'] = [[porter_stemmer.stem(word) for word in tokens] for tokens in
                                     df['tokenized_text']]

    df["sentences"] = [" ".join(tokens) for tokens in df["stemmed_tokens"]]

    return df


def get_top_data(data, top_n=5000):
    """Function to retrieve top few number of each category."""

    top_data_df_positive = data[data['sentiment'] == 1].head(top_n)
    top_data_df_negative = data[data['sentiment'] == -1].head(top_n)
    top_data_df_neutral = data[data['sentiment'] == 0].head(top_n)
    top_data_df_small = pd.concat([top_data_df_positive, top_data_df_negative, top_data_df_neutral])
    return top_data_df_small


def split_train_test(data, labels, train_size=0.7, shuffle_state=True):
    # From https://datascience.stackexchange.com/questions/15135/train-test-validation-set-splitting-in-sklearn

    assert train_size >= 0 and train_size <= 1, "Invalid training set fraction"

    X_train, X_test, Y_train, Y_test = train_test_split(data,
                                                        labels,
                                                        shuffle=shuffle_state,
                                                        train_size=train_size,
                                                        random_state=15)

    X_train, X_tmp, Y_train, Y_tmp = train_test_split(
        data, labels, train_size=train_size, random_state=15)

    # Split testing set 50%, so if train_size = 0.7, this function splits the data
    # with the ratio of 70 / 15 / 15 for training, validation, testing respectively
    X_val, X_test, Y_val, Y_test = train_test_split(
        X_tmp, Y_tmp, train_size=0.5, random_state=15)

    print("Value counts for training sentiments")
    print(Y_train.value_counts())
    print("Value counts for validation sentiments")
    print(Y_val.value_counts())
    print("Value counts for testing sentiments")
    print(Y_test.value_counts())

    X_train = X_train.reset_index()
    X_val = X_val.reset_index()
    X_test = X_test.reset_index()
    Y_train = Y_train.to_frame()
    Y_train = Y_train.reset_index()
    Y_val = Y_val.to_frame()
    Y_val = Y_val.reset_index()
    Y_test = Y_test.to_frame()
    Y_test = Y_test.reset_index()
    print(X_train.head())

    return X_train, X_val,  X_test, Y_train, Y_val, Y_test


def make_dict(top_data_df_small, padding=True):
    if padding:
        print("Dictionary with padded token added")
        review_dict = corpora.Dictionary([['pad']])
        review_dict.add_documents(top_data_df_small['stemmed_tokens'])
    else:
        print("Dictionary without padding")
        review_dict = corpora.Dictionary(top_data_df_small['stemmed_tokens'])
    return review_dict


def make_bow_vector(review_dict, sentence, vocab_size, device="cpu"):
    """Function to make bow vector to be used as input to network."""

    vec = torch.zeros(vocab_size, dtype=torch.float64, device=device)
    for word in sentence:
        vec[review_dict.token2id[word]] += 1
    return vec.view(1, -1).float()


def make_vector_using_vectorizer(vectorizer, sentence, device="cpu", dtype=None):
    # Use vectorizer to transform to vector and transform it to numpy array
    vec = np.array(vectorizer.transform([sentence]).todense())[0]
    # Transform numpy array to torch tensor
    vec = torch.from_numpy(vec)

    # Cast it to specific device torch tensor
    if dtype:
        vec = vec.to(dtype=dtype, device=device)
    else:
        vec = vec.to(device=device)
        # torch.tensor(vec, device=device)
    return vec.view(1, -1).float()


def make_target(label, device="cpu"):
    """Function to get the output tensor.

    Returns the index of the correct output of a list of distributions.
    """
    if label == -1:
        return torch.tensor([0], dtype=torch.long, device=device)
    elif label == 0:
        return torch.tensor([1], dtype=torch.long, device=device)
    else:
        return torch.tensor([2], dtype=torch.long, device=device)


def make_target_binary(label, device="cpu"):
    """Function to get the output tensor."""
    if label == -1:
        return torch.tensor([0], dtype=torch.long, device=device)
    elif label == 1:
        return torch.tensor([1], dtype=torch.long, device=device)


def train_bow_vectorizer(data, stop_words=False, save=False, filename=None):
    if stop_words:
        vectorizer = CountVectorizer(analyzer="word", stop_words="english")
    else:
        vectorizer = CountVectorizer(analyzer="word")
    vectorizer.fit(data)

    if save:
        _save_vectorizer(vectorizer, filename)

    return vectorizer


def train_tfidf_vectorizer(data, stop_words=False, save=False, filename=None):
    if stop_words:
        vectorizer = TfidfVectorizer(analyzer="word", stop_words="english")
    else:
        vectorizer = TfidfVectorizer(analyzer="word")
    vectorizer.fit(data)

    if save:
        _save_vectorizer(vectorizer, filename)

    return vectorizer


def _save_vectorizer(vectorizer, filename):
    # Save Vectorizer object
    with open("models/representation/{}_vectorizer.pk".format(filename), "wb") as f:
        pickle.dump(vectorizer, f)

    # Save word embeddings
    with open("models/representation/{}_embedding.txt".format(filename), "w") as f:
        # Output each word in the vocabulary and its corresponding vector
        for word in vectorizer.vocabulary_:
            vector = vectorizer.transform([word]).todense()
            vect_string = " ".join(str(x) for x in np.array(vector)[0])
            f.write("{} {}\n".format(word, vect_string))