"""File for different text vectorization techniques."""

import os
import numpy as np

import gensim.downloader
from gensim import utils as gen_utils
from gensim.models import TfidfModel, Word2Vec, FastText
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.corpora import Dictionary

# For ELMo
"""
import tensorflow.compat.v1 as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
"""

class TokenizedTextIter:
    def __init__(self, path):
        self.path = path

    def __iter__(self):
        with open(self.path, 'r', encoding='utf-8') as fin:
            for line in fin:
                # TODO: extra pre-processing?
                yield list(gen_utils.tokenize(line, lower=True))


class DataIter:
    def __init__(self, path):
        self.path = path

    def __iter__(self):
        with open(self.path, 'r', encoding='utf-8') as fin:
            for line in fin:
                # TODO: extra pre-processing?
                yield line.lower()


class Vectorizer:
    def __init__(self, **kwargs):
        pass

    def train(self, data_path, out_path):
        raise NotImplementedError()

    def vectorize(self, tokens):
        raise NotImplementedError()


class TfidfVectorizer(Vectorizer):
    def __init__(self, params):
        super(TfidfVectorizer, self).__init__()
        self.model = TfidfModel.load(params["path_to_model"])
        self.dct = Dictionary.load(params["path_to_dict"])

    def train(self, data_path, out_path):
        assert os.path.isdir(out_path), "'{}' is not a directory!".format(out_path)

        data_iter = DataIter(data_path)

        print("Building dictionary...")
        self.dct = Dictionary(data_iter)
        print("Building TF-IDF vectorizer...")
        corpus = [self.dct.doc2bow(line) for line in data_iter]
        self.model = TfidfModel(corpus)
        print("Saving model dictionary to '{}/tfidf_vectorizer.dict'".format(out_path))
        self.dct.save("{}/tfidf_vectorizer.dict".format(out_path))
        print("Saving vectorizer model to '{}/tfidf_vectorizer.model'".format(out_path))
        self.model.save("{}/tfidf_vectorizer.model".format(out_path))

    def vectorize(self, tokens):
        tokens = self.dct.doc2bow(tokens)
        vector = []
        for _, value in self.model[tokens]:
            vector.append(value)
        return vector


class WordVectorizer(Vectorizer):
    def __init__(self, params):
        super(WordVectorizer, self).__init__()
        if "pretrained" in params:
            self.keyed_vecs = gensim.downloader.load(params["pretrained"])

    def train(self, data_path, out_path):
        raise NotImplementedError()

    def vectorize(self, tokens):
        """Vectorize a sentence by averaging out word embeddings."""

        vectors = []

        # Split sentence into word tokens
        # words = list(tokenize(tokens))

        # For each word in the sentence...
        for token in tokens:
            # Get its vector using the model
            try:
                vectors.append(self.keyed_vecs[token])
            except KeyError:
                # If we encounter an out-of-vocabulary word, skip it
                # TODO: implement a better way to handle this (random vector, UNK vector?)
                pass

        # Compute mean element-wise
        return np.mean(vectors, axis=0)


class Word2VecVectorizer(WordVectorizer):
    def __init__(self, params):
        super(Word2VecVectorizer, self).__init__(params)
        if "path_to_model" in params:
            self.model = Word2Vec.load(params["path_to_model"])
            self.keyed_vecs = self.model.wv

    def train(self, data_path, out_path, size=100, window=5, epochs=30):
        data_iter = DataIter(data_path)

        self.model = Word2Vec(size=size, window=window)
        print("Building vocab...")
        self.model.build_vocab(sentences=data_iter)
        total_examples = self.model.corpus_count
        print("Training Word2Vec vectorizer...")
        self.model.train(sentences=data_iter, total_examples=total_examples, epochs=epochs)
        print("Saving vectorizer model as '{}/word2vec_vectorizer.model'".format(out_path))
        self.model.save("{}/word2vec_vectorizer.model".format(out_path))
        self.keyed_vecs = self.model.wv


class FastTextVectorizer(WordVectorizer):
    def __init__(self, params):
        super(FastTextVectorizer, self).__init__(params)
        if "path_to_model" in params:
            self.model = FastText.load(params["path_to_model"])
            self.keyed_vecs = self.model.wv

    def train(self, data_path, out_path, size=100, window=5, epochs=30):
        data_iter = DataIter(data_path)

        self.model = FastText(size=size, window=window)
        print("Building vocab...")
        self.model.build_vocab(sentences=data_iter)
        total_examples = self.model.corpus_count
        print("Training FastText vectorizer...")
        self.model.train(sentences=data_iter, total_examples=total_examples, epochs=epochs)
        print("Saving vectorizer model to '{}/fasttext_vectorizer.model'".format(out_path))
        self.model.save("{}/fasttext_vectorizer.model".format(out_path))
        self.keyed_vecs = self.model.wv


class Doc2VecVectorizer(Vectorizer):
    def __init__(self, params):
        super(Doc2VecVectorizer, self).__init__()
        if "path_to_model" in params:
            self.model = Doc2Vec.load(params["path_to_model"])
            self.keyed_vecs = self.model.wv

    def train(self, data_path, out_path, vector_size=100, window=5):
        data_iter = DataIter(data_path)

        print("Building documents...")
        documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(data_iter)]
        print("Training Doc2Vec vectorizer...")
        self.model = Doc2Vec(documents, vector_size=vector_size, window=window)
        print("Saving model to '{}/doc2vec_vectorizer.model'".format(out_path))
        self.model.save("{}/doc2vec_vectorizer.model".format(out_path))
        self.keyed_vecs = self.model.wv

    def vectorize(self, tokens):
        return self.model.infer_vector(tokens)


class ELMoVectorizer(Vectorizer):
    def __init__(self, params):
        # Only works with tensorflow version 1.15
        import tensorflow_hub as hub
        super(ELMoVectorizer, self).__init__()

        self.model = hub.Module(params["path_to_model"], trainable=False)

    def train(self, data_path, out_path):
        raise NotImplementedError("Training would be very expensive but maybe I'll implement this in the future")

    def vectorize(self, tokens):
        """
        sentence = " ".join(tokens)
        word_embeddings = self.model([sentence], signature="default", as_dict=True)["elmo"]
        with tf.Session(config=config) as sess:
            sess.run(tf.global_variables_initializer())
            embeddings = sess.run(word_embeddings)[0]
        avg_embeddings = np.mean(embeddings, axis=0)
        return avg_embeddings
        """
        pass


class InferSentVectorizer(Vectorizer):
    def __init__(self, params):
        from utils.InferSent.models import InferSent
        from utils.networks import is_cuda
        import torch
        super(InferSentVectorizer, self).__init__()

        sentences = params["sentences"]
        path_to_model = params["path_to_model"]
        params_model = params["params_model"]
        path_to_w2v = params["path_to_w2v"]
        if not params_model:
            params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                            'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}
            path_to_model = "InferSent/encoder/infersent2.pkl"
            print("Overwriting path to InferSent model to '{}'".format(path_to_model))

        print("Loading InferSent model...")
        self.model = InferSent(params_model)
        self.model.load_state_dict(torch.load(path_to_model))
        self.model.set_w2v_path(path_to_w2v)

        self.model = self.model.to(is_cuda())
        print("Building InferSent vocabulary...")
        self.model.build_vocab(sentences, tokenize=True)

    def train(self, data_path, out_path):
        raise NotImplementedError("Training would be very expensive but maybe I'll implement this in the future")

    def vectorize(self, tokens):
        sentence = " ".join(tokens)
        return self.model.encode(sentence)[0]


class USEVectorizer(Vectorizer):
    def __init__(self, params):
        # Works with tensorflow version 2.x
        import tensorflow_hub as hub
        super(USEVectorizer, self).__init__()

        if "path_to_model" in params:
            self.model = hub.load(params["path_to_model"])

    def train(self, data_path, out_path):
        raise NotImplementedError("Training would be very expensive but maybe I'll implement this in the future")

    def vectorize(self, tokens):
        sentence = " ".join(tokens)
        return self.model([sentence])[0].numpy()


class TransformerVectorizer(Vectorizer):
    def __init__(self, params):
        from sentence_transformers import SentenceTransformer
        super(TransformerVectorizer, self).__init__()

        if "path_to_model" in params:
            self.model = SentenceTransformer(params["path_to_model"])

    def train(self, data_path, out_path):
        raise NotImplementedError("Training would be very expensive but maybe I'll implement this in the future")

    def vectorize(self, tokens):
        sentence = " ".join(tokens)
        return self.model.encode([sentence])[0]


class AutoEncoderVectorizer(Vectorizer):
    def __init__(self, path_to_model=None):
        super(AutoEncoderVectorizer, self).__init__()

    def train(self, data_path, out_path):
        raise NotImplementedError("Training would be very expensive but maybe I'll implement this in the future")

    def vectorize(self, tokens):
        sentence = " ".join(tokens)
        return self.model.encode([sentence])[0]


if __name__ == "__main__":
    # imdb_iter = DataIter("../external-repos/txt-autoencoders/data/imdb/train_imdb_without_sentiment.txt")
    tokens = list(gen_utils.tokenize("This is my favourite movie by far", lower=True))
    #tfidf = TfidfVectorizer("../models/representation/tfidf_vectorizer.model",
    #                        "../models/representation/tfidf_vectorizer.dict")
    vectorizer = TransformerVectorizer("paraphrase-distilroberta-base-v1")
    # vectorizer = InferSentVectorizer(imdb_iter)
    vect = vectorizer.vectorize(tokens)
    print(tokens)
    print(vect)
