import torch
import onnx


def torch_to_onnx(model, batch_size, input_dim, name, device="cpu", sample_input=None):
    # Set the model to inference mode
    # This is required since operators like dropout
    # or batchnorm behave differently in inference and training mode.
    model.eval()

    if type(sample_input) == type(None):
        # Input to the model
        sample_input = torch.randn(batch_size, input_dim, requires_grad=True, device=device)

    torch_out = model(sample_input)

    name = "models/onnx/{}.onnx".format(name)

    # Export the model
    torch.onnx.export(model,  # model being run
                      sample_input,  # model input (or a tuple for multiple inputs)
                      name,  # where to save the model (can be a file or file-like object)
                      export_params=True,  # store the trained parameter weights inside the model file
                      opset_version=10,  # the ONNX version to export the model to
                      do_constant_folding=True,  # whether to execute constant folding for optimization
                      input_names=['input'],  # the model's input names
                      output_names=['output'],  # the model's output names
                      verbose=False,  # Export protobuffer in human-readable form
                      dynamic_axes={'input': {0: 'batch_size'},  # variable lenght axes
                                    'output': {0: 'batch_size'}})

    print("Successfully exported to {}".format(name))


def print_onnx_graph(onnx_filename):
    testnet_onnx = onnx.load(onnx_filename)
    # Print a human readable representation of the graph
    print(onnx.helper.printable_graph(testnet_onnx.graph))
