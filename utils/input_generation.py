import spacy
from collections import defaultdict
import math
import random


class BaseText:
    def __init__(self, text):
        self.text = text
        self.pos_tags = self._compute_pos_tags()

    def _compute_pos_tags(self):
        text = self.text.split()
        pos_tags = []
        for word in text:
            if word in POS_TAGS:
                pos_tags.append(word)

        return pos_tags

    def remove_pos_tag(self, tag):
        if "<" not in tag:
            tag = "<{}>".format(tag)

        self.pos_tags.remove(tag)


# From https://universaldependencies.org/docs/u/pos/
"""
ADJ: adjective
ADP: adposition
ADV: adverb
AUX: auxiliary verb
CONJ: coordinating conjunction
DET: determiner
INTJ: interjection
NOUN: noun
NUM: numeral
PART: particle
PRON: pronoun
PROPN: proper noun
PUNCT: punctuation
SCONJ: subordinating conjunction
SYM: symbol
VERB: verb
X: other
"""

POS_TAGS = ["<ADJ>", "<ADP>", "<ADV>", "<AUX>", "<CONJ>", "<DET>", "<INTJ>", "<NOUN>",
            "<NUM>", "<PART>", "<PRON>", "<PROPN>", "<PUNCT>", "<SCONJ>", "<SYM>", "<VERB>", "<X>"]

# Load spaCy
nlp = spacy.load("en_core_web_sm")

# TODO: need article before noun
input_str = BaseText("I really <VERB> this <ADJ> movie")
#input_str = BaseText("<NOUN> <ADJ> <VERB>")
#base_n = BaseText("This <NOUN> <ADJ> movie is <NOUN>")
#base_verb = BaseText("I <VERB> this movie")
#base_adj = BaseText("This movie is <ADJ>")

verbose = True

vocab_cap = 40

corpora_file_name = "data/positive-words.txt"
# corpora_file_name = "data/negative-words2.txt"
sentiment = 1
gen_file_name = "pos_inputs"

pos_dict = defaultdict(list)

with open(corpora_file_name, "r") as corpora_file:
    for line in corpora_file:
        if line[0] == ";" or line[0] == "\n":
            continue
        # FIXME: so far only one word at a time is processed
        word = nlp(line)
        word = word[0]
        # or?
        # pos_dict[word.pos_] = word.text
        pos = "<{}>".format(word.pos_)
        pos_dict[pos].append(word.text.lower())

# TODO: do this when generating sentences so more variance is introduced
# Cap the vocabulary for each POS
for key in pos_dict.keys():
    words = pos_dict[key]
    if len(words) > vocab_cap:
        # Sample the vocabulary
        words = random.sample(words, vocab_cap)
        pos_dict[key] = words

num_permutations = len(pos_dict[input_str.pos_tags[0]])
for pos_tag in input_str.pos_tags[1:]:
    num_permutations = num_permutations * len(pos_dict[pos_tag])

print("Capped no. of permutations: {}".format(num_permutations))

idx = 1

for pos_tag in input_str.pos_tags:
    end = idx == len(input_str.pos_tags)
    extension = "csv" if end else "txt"

    if idx == 1:
        with open("data/{}{}.{}".format(gen_file_name, idx, extension), "w") as write_file:
            if end:
                write_file.write("text,label\n")

            temp_test = input_str.text

            for word in pos_dict[pos_tag]:
                gen_sent = temp_test.replace(pos_tag, word)
                if verbose:
                    print(gen_sent)
                if end:
                    write_file.write("\"{}\",{}\n".format(gen_sent, sentiment))
                else:
                    write_file.write("{}\n".format(gen_sent))
    else:
        with open("data/{}{}.txt".format(gen_file_name, idx-1), "r") as read_file:
            with open("data/{}{}.{}".format(gen_file_name, idx, extension), "w") as write_file:
                if end:
                    write_file.write("text,label\n")

                for line in read_file:
                    # Take out newlines
                    line = line.strip()

                    for word in pos_dict[pos_tag]:
                        gen_sent = line.replace(pos_tag, word)
                        if verbose:
                            print(gen_sent)
                        if end:
                            write_file.write("\"{}\",{}\n".format(gen_sent, sentiment))
                        else:
                            write_file.write("{}\n".format(gen_sent))

    idx += 1
